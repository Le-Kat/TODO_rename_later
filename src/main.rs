// supress stylistic warnings
#![allow(unused_parens)]
//#![allow()]

mod json;

use std::io;
use std::io::Write;
use std::fs;
use std::collections;
use std::path;

use jq_rs;
use youtube_dl;
use serde_json;
use regex;

const ID_LIST: [&str; 38] = ["UszZG-9zZzg", "z2bqF_Sv2UY", "hh1qHvCEC5o", "OaKZcmZdscM", "Qo6lD81osxs", "SvoQKBy41rI", "URrjzaGc--E", "E72skAVzN6U", "MSISZTvFMFA", "tdvfz_AteAk", "K2KcpsFuohA", "qtXnjvzPBHU", "Ww7t3FyYL5s", "b_QZOuBddqo", "wo4Y-43Bh8o", "vFtq4Mg9Ak8", "-2jIdPxZBDk", "67QgffrFBLU", "1BBP8D9OYtg", "iPsCjFOZ2dg", "SLj1fqg6jyU", "Zd5xDL11EYg", "fAZXG7HHzsY", "534y8I8xkko", "71HeNaE0vmE", "OeEifEHFa9E", "HNN_pgfERbw", "v_vn3RVdpYY", "o9GWfthjn_0", "HCKzC2Lp-yc", "2zftaaa4bEk", "DwtFD0cMkzg", "uRnkK6rRk_0", "x-5PBlEyckY", "GycgC6yTNQ8", "jEqwh6Qr3zA", "0TrKDOokSf8", "tia-Tq41tRE"];
const JQ_COMMAND: &str = ".replayChatItemAction.actions.[0].addChatItemAction.item.liveChatTextMessageRenderer | {author: .authorName.simpleText, message: .message.runs [0].text} | select (.message)";

fn main() {
	
	let mut stderr: io::Stderr = io::stderr();
	let r: regex::Regex = regex::RegexBuilder::new (r"pee\s?pee\s?poo\s?poo")
		.case_insensitive (true)
		.build()
		.unwrap();
	
	for id in ID_LIST {
		
		download (id);
	}
	
	let mut flag: bool = false;
	let mut val: f32 = 0f32;
	let mut count: u32 = 0;
	
	print! ("[");
	for id in ID_LIST {
		
		let messages: Vec <json::Message> = file_to_arr (id);
		let mut targets: collections::HashMap <String, bool> = collections::HashMap::new();
		
		for m in messages {
			
			if (targets.len() > 0) {
				
				if (targets.contains_key (&m.author)) {
					
					*targets.get_mut (&m.author).unwrap() = true;
				}
			}
			
			if (r.is_match (&m.message)) {
				
				targets.insert (m.author, false);
			}
		}
		
		let mut t: u32 = 0;
		for (_k, v) in &targets {
			
			if (*v == true) {
				
				t += 1;
			}
		}
		
		if (targets.len() > 0) {
			
			val += (t as f32 / targets.len() as f32);
			count += 1;
			
			if (flag) {
				
				print! (r#",{{"videoID": "{}","returnRatio": {}}}"#, id, (t as f32 / targets.len() as f32));
			} else {
				
				flag = true;
				print! (r#"{{"videoID": "{}","returnRatio": {}}}"#, id, (t as f32 / targets.len() as f32));
			}
		}
	}
	println! ("]");
	
	writeln! (&mut stderr, "total return ratio: {}%", ((100f32 * val) / (count as f32))).unwrap();
}

fn download (id: &str) {
	
	let mut stderr: io::Stderr = io::stderr();
	
	if (path::Path::new (&format! ("files/{}.live_chat.json", id)).exists()) {
		
		writeln! (&mut stderr, "file '{}' already downloaded", id).unwrap();
		return;
	}
	
	writeln! (&mut stderr, "downloading '{}'", id).unwrap();
	
	let _ = youtube_dl::YoutubeDl::new (&format! ("https://www.youtube.com/watch?v={}", id))
		.socket_timeout ("15")
		.output_template ("%(id)s")
		.extra_arg ("--skip-download")
		.extra_arg ("--write-sub")
		.download_to ("files");
}

fn file_to_arr (id: &str) -> Vec <json::Message> {
	
	let file_name: &str = &format! ("files/{}.live_chat.json", id);
	let mut messages: Vec <json::Message> = Vec::new();
	let mut parse = jq_rs::compile (JQ_COMMAND).unwrap();
	
	for line in fs::read_to_string (file_name).unwrap().lines() {
		
		let out = parse.run (line).unwrap();
		
		if (out.len() > 0) {
			
			let m: json::Message = serde_json::from_str (&out).unwrap();
			messages.push (m);
		}
	}
	
	return (messages);
}
