// supress stylistic warnings
#![allow(unused_parens)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
//#![allow()]

use serde::{Serialize, Deserialize};

#[derive (Serialize, Deserialize, Debug)]
pub struct Message {
	
	pub author: String,
	pub message: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct Return_object {
	
	pub clickTrackingParams: String,
	pub replayChatItemAction: replayChatItemAction,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct replayChatItemAction {
	
	pub actions: Vec <action>,
	pub videoOffsetTimeMsec: u64,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct action {
	
	pub clickTrackingParams: String,
	pub addChatItemAction: addChatItemAction,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct addChatItemAction {
	
	pub item: item,
	pub clientId: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct item {
	
	pub item: liveChatViewerEngagementMessageRenderer,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct liveChatViewerEngagementMessageRenderer {
	
	pub id: String,
	pub timestampUsec: u64,
	pub icon: icon,
	trackingParams: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct icon {
	
	pub iconType: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct message {
	
	pub runs: Vec <run>,
	pub authorName: authorName,
	pub authorPhoto: authorPhoto,
	pub contextMenuEndpoint: contextMenuEndpoint,
	pub id: String,
	pub timestampUsec: u64,
	pub authorBadges: Vec <authorBadge>,
	pub authorExternalChannelId: String,
	pub contextMenuAccessibility: contextMenuAccessibility,
	pub timestampText: timestampText,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct run {
	
	pub text: Option <String>,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct emoji {
	
	pub emojiId: String,
	pub shortcuts: Vec <String>,
	pub searchTerms: Vec <String>,
	pub image: image,
	pub isCustomEmoji: bool,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct image {
	
	pub thumbnails: Vec <bool>,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct thumbnail {
	
	pub url: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct authorName {
	
	simpleText: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct authorPhoto {
	
	pub thumbnails: Vec <thumbnail>,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct contextMenuEndpoint {
	
	pub commandMetadata: commandMetadata,
	pub liveChatItemContextMenuEndpoint: liveChatItemContextMenuEndpoint,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct commandMetadata {
	
	pub webCommandMetadata: webCommandMetadata,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct webCommandMetadata {
	
	pub ignoreNavigation: bool,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct liveChatItemContextMenuEndpoint {
	
	pub params: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct authorBadge {
	
	pub liveChatAuthorBadgeRenderer: liveChatAuthorBadgeRenderer,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct liveChatAuthorBadgeRenderer {
	
	pub customThumbnail: customThumbnail,
	pub tooltip: String,
	pub accessibility: accessibility,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct customThumbnail {
	
	pub thumbnails: Vec <thumbnail>,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct accessibility {
	
	pub accessibilityData: accessibilityData,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct accessibilityData {
	
	label: String,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct contextMenuAccessibility {
	
	pub accessibilityData: accessibilityData,
}

#[derive (Serialize, Deserialize, Debug)]
pub struct timestampText {
	
	simpleText: String,
}
